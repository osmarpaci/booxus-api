var https = require('https');
const { token, domain } = require('../../constant');
const { makeRequest, dynamicSort } = require('../../common');

//https://my.sevdesk.de/api/v1/Invoice/9250376/getCheckAccountTransactions?token=051c52cc513d911e88f4a66ee1e8ee26

exports.getPayments = async function(req, res, next) {
    const { name } = req.params;
    try {
        const result = await makeRequest(`${domain}/api/v1/CheckAccountTransaction?limit=10000&offset=0&token=${token}`);
        res.json(result.objects);
    } catch(error) {
      console.log(error);
    } 
};

exports.getPaymentList = async function(req, res, next) {
   const { name, account } = req.params;

   try{

        const url = `${domain}/api/v1/CheckAccountTransaction?limit=10000&offset=0&embed=checkAccount&token=${token}`;
        https.get(url, 
        function (response) {
        var buffer = "";
    
        response.on("data", function (chunk) {
            buffer += chunk;
        }); 

        response.on("end", function (err) {
            const data = JSON.parse(buffer);
            
            var result = [];

            data["objects"].forEach(element => {
                const {object } = element;
               
                if (name !== 'null' && account !== 'null') {
                 if (name == element.payeePayerName && account == element.checkAccount.name ) 
                   {
                        result.push(element);

                   }
                }
                else
                {
         
                }


            });
            res.json(result);
        }); 
        });




       } catch(error) {
      console.log(error);
    }

};


exports.getinvoiceCheckAccountTransactions = async function(req, res, next) {
    const { invoice_id } = req.params;
//    console.log(invoice_id);
    try {
        const result = await makeRequest(`${domain}/api/v1/Invoice/${invoice_id}/getCheckAccountTransactions?&token=${token}`);
        res.json(result.objects);
    } catch(error) {
      console.log(error);
    } 
};


