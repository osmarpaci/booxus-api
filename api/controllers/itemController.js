var https = require('https');
var request = require('request');
const var_dump = require('var_dump');
const { token, domain } = require('../../constant');
const { makeRequest, getDate, getUrlVars, dynamicSort, newOrder} = require('../../common');
const { parts} = require('../../parts');
const { edeka} = require('../../edeka');


exports.item = function(req, res, next) {
    const { search } = req.params;
    var result = [];

    try {
        https.get(`${domain}/api/v1/Part?token=${token}`, 
            function (response) {
            var buffer = "";
        
            response.on("data", function (chunk) {
                buffer += chunk;
            }); 
        
            response.on("end", function (err) {
                const data = JSON.parse(buffer);

                data['objects'].forEach(element => {
                    const { id, name, partNumber, taxRate, price } = element;
                    const tmp = {
                        id, name, partNumber, taxRate, price
                    };
                    if (element.stockEnabled == '1')
                    {
                        if (search) {
                            if (name.toLowerCase().indexOf(search.toLowerCase()) !== -1) {
                                result.push(tmp);
                            } 
                        }
                        else 
                        {
                            result.push(tmp);
                        }
                    }
                });
                res.json(result);
            }); 
        });
    }
    catch(e) {
        console.log('item',e);
        res.json(result);
    }
};

exports.barkod = function(req, res, next) {
    const { barkod } = req.params;
    var result = [];

    try {
        https.get(`${domain}/api/v1/Part?token=${token}`, 
            function (response) {
            var buffer = "";
        
            response.on("data", function (chunk) {
                buffer += chunk;
            }); 
        
            response.on("end", function (err) {
                const data = JSON.parse(buffer);

                data['objects'].forEach(element => {
                    const { id, internalComment, name, partNumber, taxRate, price } = element;
                    const tmp = {
                        id, name, partNumber, taxRate, price
                    };
                    
                    if (barkod) {
                        if (internalComment === barkod) {
                            result.push(tmp);
                        } 
                    }
                    

                });
                res.json(result);
            }); 
        });
    }
    catch(e) {
        console.log('item',e);
        res.json(result);
    }
};

exports.shortCode = function(req, res, next) {
    const { shortCode } = req.params;
    console.log(shortCode);
    var result = [];

    try {

                parts.forEach(element => { 
                    if (shortCode) {
                        if (element.shortCode === shortCode) {
                            result.push(element);
                        } 
                    }
                    

                });
                res.json(result);

    }
    catch(e) {
        console.log('item',e);
        res.json(result);
    }
};

exports.barCode = function(req, res, next) {
    const { barCode } = req.params;
    console.log(barCode);
    var result = [];

    try {

                parts.forEach(element => { 
                    if (barCode) {
                        if (element.barcode === barCode) {
                            result.push(element);
                            res.json(result);
                        } 
                    }
                    

                });
                

    }
    catch(e) {
        console.log('item',e);
        res.json(result);
    }
};

exports.barCodeFill = function(req, res, next) {
    const { id, barCode, priceGross } = req.params;
    console.log(id, barCode);
    var result = [];
    var partNumber = barCode,brand="",internalComment="",text="",taxRate="",text='',shortCode="",subCategory= "",name= "", imageuri = "",categories = "";

    try {


                if (result == null || result=='')
                {   
//-------------------------------------------------- edeka -------------------------------------

                          edeka.forEach(element => { 
                            
                                    if (element.barcode == barCode) {
                                        partNumber = id +'-'+ barCode;  
                                        brand = element.brand;
                                        shortCode = barCode ;//data['product'].code;
                                        subCategory = element.subCategory;
                                        name   = element.name;
                                        imageuri = element.imageuri;
                                        categories = element.category;

                                        const tmp = {
                                             id, internalComment,text,taxRate,text,partNumber, priceGross, brand, shortCode, subCategory, name, imageuri, categories
                                        };
                                        result.push(tmp);
                                        res.json(result);
                                        } 
                            
                            });

//-------------------------------------------------- open food facts  -------------------------------------

                    if(result.length > 0) // edeka listeesinde var ise
                    {
                         res.json(result);
                    }
                    else   // openfoodfacts e bak
                    {

                            const url_off = `https://world.openfoodfacts.org/api/v0/product/${barCode}.json`;
                            console.log(url_off); 
                                        try {
                                                https.get(url_off, 
                                                    function (response) {
                                                    var buffer = "";
                                                
                                                    response.on("data", function (chunk) {
                                                        buffer += chunk;
                                                    }); 
                                                
                                                    response.on("end", function (err) {
                                                        const data = JSON.parse(buffer);
                    /*                                    result.push(data);
                                                        res.json(result);*/
                                                    
                                                       if (data.status =='1')
                                                        {
                                                            console.log(data['product'].image_url);
                                                            partNumber = id +'-'+ data['product'].code;  
                                                            brand = data['product'].brands;
                                                            shortCode = barCode ;//data['product'].code;
                                                            subCategory = data['product'].categories;
                                                            name   = data['product'].product_name_de;
                                                            imageuri = data['product'].image_url;
                                                            categories = data['product'].categories;
                                                            
                                                            const tmp = {
                                                                 id, internalComment,text,taxRate,text,partNumber, priceGross, brand, shortCode, subCategory, name, imageuri, categories
                                                            };
                                                            result.push(tmp);
                             
                                                         }
                                                         else
                                                         {
                                                            const tmp = {
                                                                 id, internalComment,text,taxRate,text,partNumber, priceGross, brand, shortCode, subCategory, name, imageuri, categories
                                                            };
                                                            result.push(tmp);
                                                         }   
                                                        
                                                    }); 
                                                });
                                            }
                                            catch(e) {
                                                console.log('item',e);
                                            }
//-------------------------------------------------- buycott -------------------------------------

                            if(result.length > 0) // open food facts de var ise
                            {
                                res.json(result);
                            }
                            else   // buycott a bak
                            {

                                                    const url_buycott = `'barcode': '${barCode}', 'access_token': 'RgueGej8-zX-jH_uYgL14-ZNOe2B0C64_C_Q_A6b' `;
                                                    console.log(url_buycott);

                                                    const axios = require('axios')

                                                    axios.post('https://www.buycott.com/api/v4/products/lookup', {
                                                      'barcode': barCode,
                                                      'access_token':'RgueGej8-zX-jH_uYgL14-ZNOe2B0C64_C_Q_A6b'
                                                    })
                                                    .then((res) => {
                                                      console.log("Sonuc : ", res.data)
                                                    })
                                                    .catch((error) => {
                                                      console.error("Hata : ", error)
                                                    })

                        
                            }


                    }
                }

                

    }
    catch(e) {
        console.log('item',e);
        res.json(result);
    }
};

exports.itemCode = function(req, res, next) {
    const { itemId } = req.params;
    var result = [];
  
    try {
        https.get(`${domain}/api/v1/Part?token=${token}`, 
            function (response) {
            var buffer = "";
        
            response.on("data", function (chunk) {
                buffer += chunk;
            }); 
        
            response.on("end", function (err) {
                const data = JSON.parse(buffer);

                data['objects'].forEach(element => {
                    const { id } = element;           
                    if (itemId) {
                        if (id === itemId) {
                            result.push(element);
                        } 
                    }
                });
                res.json(result);
            }); 
        });
    }
    catch(e) {
        console.log('item',e);
        res.json(result);
    }
};

exports.getStock = function(req, res, next) {
    const { itemId, maxDate } = req.params;
    var result = [];

    try {
        https.get(`${domain}/api/v1/Part/${itemId}/getStock?maxDate=${maxDate}?token=${token}`, 
            function (response) {
            var buffer = "";
        
            response.on("data", function (chunk) {
                buffer += chunk;
            }); 
        
            response.on("end", function (err) {
                const data = JSON.parse(buffer);

                data['objects'].forEach(element => {
                     result.push(element);
                });
                res.json(result);
            }); 
        });
    }
    catch(e) {
        console.log('item',e);
        res.json(result);
    }
};

exports.itemCategory = function(req, res, next) {
    const { searchCategory } = req.params;
    var result = [];
    try {
                    edeka.forEach(element => { 
//                    console.log(element) ;    
                    const {subCategory } = element;              
                    // if (element.stockEnabled == '1')
                    // {
                        if (searchCategory) {
                            if (searchCategory == subCategory) {
                                result.push(element);
                            } 
                        }
                    // }
                });
                res.json(result);
    }
    catch(e) {
        console.log('item',e);
        res.json(result);
    }
};



exports.itemInvoicePositions = function(req, res, next) {
    const { search,type } = req.params;
    var result = [];
    const url = `${domain}/api/v1/InvoicePos?limit=10000&offset=0&embed=part,invoice&token=${token}`;
     console.log (url);
    try {
 
             https.get(url, 
            function (response) {
            var buffer = "";
        
            response.on("data", function (chunk) {
                buffer += chunk;
            }); 
        
            response.on("end", function (err) {
                const data = JSON.parse(buffer);

                   data['objects'].forEach(element => {

                   if (element.update > '2020-05-10T00:00:00+01:00' && (element.text ==='' || element.text ===null) && element.part !== undefined){
                       //&& (element.text ==='' || element.text ===null) && element.part.category.id !== '140050'
                        if (type =="Itm-") {
                            if (search == element.part.id) {
                                result.push(element);
                            } 
                        }
                        else if (type == "Cat-")
                        {
                            if (search == element.part.category.name) {
                                result.push(element);
                            } 
                        }
                        else
                            {
                            result.push(element);
                            }
                    }
               
                });
                res.json(result);
            }); 
        });
    }
    catch(e) {
        console.log('item',e);
        res.json(result);
    }
};


exports.itemOrderPositions =  async function(req, res, next) {
    const { search,type,orderType,date } = req.params;
    var result = [], list=[], total=[];
    let tmpProducerID="",tmpProducer="",tmpSupplierCode;
    let ProducerID="",Producer="",supplierCode="", ItemCode="", PartNumber="", Name="", Stock=0, Qty=0,Price=0,Tax=0,demandId="",order="";
    let Pos="",PosId='',PosNorder="", PosX="", PosItemCode='', PosItemName='';

    const url = `${domain}/api/v1/OrderPos?limit=10000&offset=0 &orderBy[0][field]=part&orderBy[0][arrangement]=desc&orderBy[1][field]=update&orderBy[1][arrangement]=desc &embed=part,order&token=${token}`;

    try {
 
             https.get(url, 
            function (response) {
            var buffer = "";
        
            response.on("data", function (chunk) {
                buffer += chunk;
            }); 
        
            response.on("end", function (err) {
                const data = JSON.parse(buffer);
                const son_element=[];
                   data['objects'].forEach(element => {


                   if ( element.part !== undefined && 
                        orderType === element.order.orderType && 
//                        element.order.status === "100" &&
                        element.create > '2020-05-21T00:00:00+01:00' &&
                        element.order.customerInternalNote === null &&
                        (element.text === null || element.text === '_' || element.text === '' ) 
                        //xyz yi deneme datalarında kullanmak için sonra silinebilir
                        )
                   {            

                            parts.forEach(prt => {
                                if(prt.partNumber == element.part.partNumber)
                                {
                                let {logolink,producer,producerId,imageuri,description,supplierCode} =prt;
                                tmpProducer='Edeka Alt-Rudow'; //producer;
                                tmpProducerID='22053738'; //producerId;
                                tmpSupplierCode=supplierCode;
                                }

                             });        

/* Edeka dan sonra açılacak
                           if (search !== "_") {
                               if (search !== tmpProducerID) {return;}
                           }
*/
                            PosId = element.id; 
                            PosNorder = '';
                            PosItemCode = element.part.id;
                            PosItemName = element.part.name;
                            PosX = {PosId,PosItemCode,PosItemName,PosNorder};
                            list.push(PosX);
                            //if (Posx.length>0) {son_element.splice(0,Posx.length);}                                

                        if (ItemCode === element.part.id) 
                        {   
                       
                            Name=element.part.name;
                            PartNumber=element.part.partNumber;
                            Price=element.part.pricePurchase;
                            Tax=element.part.taxRate;
                            ItemCode=element.part.id;
                            demandId=element.order.id;
                            Stock= element.part.stock;
                            Qty= parseInt(Qty,10) +parseInt(element.quantity,10);
                            Pos+=','+element.id+'-'+ProducerID;

                            console.log(element.part.name, element.quantity,Qty,element.order.addressName,element.order.id,Pos);
                        }
                        else if(ItemCode ==="") 
                        {   
                            ProducerID='22053738'; //tmpProducerID;
                            Producer='Edeka Alt-Rudow'; //tmpProducer;
                            supplierCode=tmpSupplierCode;                       
                            Name=element.part.name;
                            PartNumber=element.part.partNumber;
                            Price=element.part.pricePurchase;
                            Tax=element.part.taxRate;
                            ItemCode=element.part.id;
                            demandId=element.order.id;
                            Stock= element.part.stock;
                            Qty=parseInt(element.quantity,10);
                            Pos=element.id+'-'+ProducerID;
                            console.log(element.part.name, element.quantity,Qty,element.order.addressName,element.order.id,Pos);
                        }
                        else 
                        {      
/*                                if(tmpProducerID !== ProducerID) { // cari değişmişse
                                  console.log(ProducerID,orderType);
                                  const id =  newOrder(element.order.orderType,ProducerID);
                                  console.log(id);
                                  tmpProducerID=ProducerID;
                                } */ 

                                order ={ProducerID,Producer,supplierCode,ItemCode,PartNumber,Name,Stock,Qty,Price,Tax,demandId,Pos};
                                result.push(order);


                            ProducerID='22053738'; //tmpProducerID;
                            Producer='Edeka Alt-Rudow'; //tmpProducer;
                            supplierCode=tmpSupplierCode;
                            Name=element.part.name;
                            PartNumber=element.part.partNumber;
                            Price=element.part.pricePurchase;
                            Tax=element.part.taxRate;
                            ItemCode=element.part.id;
                            demandId=element.order.id;
                            Stock= element.part.stock;
                            Qty=parseInt(element.quantity,10);
                            Pos=element.id+'-'+ProducerID;
                            console.log(element.part.name, element.quantity,Qty,element.order.addressName,element.order.id,Pos);
                    }
                    }

                });
                   if(ItemCode !=="") //son element için
                   {
                     ProducerID='22053738'; //tmpProducerID;
                     Producer='Edeka Alt-Rudow'; //tmpProducer;
                     order ={ProducerID,Producer,supplierCode,ItemCode,PartNumber,Name,Stock,Qty,Price,Tax,demandId,Pos};
                     result.push(order);
                    }           
//                result.sort(dynamicSort("ProducerID"));
                total.push(result);
                total.push(list);
                res.json(total);
            }); 
        });
    }
    catch(e) {
        console.log('item',e);
        res.json(result);
    }
};

exports.XitemOrderPositions =  async function(req, res, next) {
    const { search,type,orderType,date } = req.params;
    var result = [], list=[], total=[];
    let tmpProducerID="",tmpProducer="";
    let ProducerID="",Producer="",ItemCode="", PartNumber="", Name="", Qty=0,Price=0,Tax=0,demandId="",order="";
    let Pos="",PosId='',PosNorder="", PosX="", PosItemCode='', PosItemName='';

    const url = `${domain}/api/v1/OrderPos?limit=10000&offset=0 &orderBy[0][field]=part&orderBy[0][arrangement]=desc&orderBy[1][field]=update&orderBy[1][arrangement]=desc &embed=part,order&token=${token}`;

    try {
 
             https.get(url, 
            function (response) {
            var buffer = "";
        
            response.on("data", function (chunk) {
                buffer += chunk;
            }); 
        
            response.on("end", function (err) {
                const data = JSON.parse(buffer);
                const son_element=[];
                   data['objects'].forEach(element => {

/*                   if ( element.part !== undefined && 
                        orderType === element.order.orderType && 
                        element.order.status === "100" &&
                        element.update > '2020-02-13T00:00:00+01:00' &&
                        (element.order.customerInternalNote === null ||
                        element.order.customerInternalNote === "xyz") //xyz yi deneme datalarında kullanmak için sonra silinebilir
                        )
                   {*/

                   if ( element.part !== undefined && 
                        orderType === element.order.orderType && 
//                        element.order.status === "100" &&
                        element.update > '2020-03-01T00:00:00+01:00' &&
                        element.order.customerInternalNote === null &&
                        (element.text === null || element.text === '_' ) //xyz yi deneme datalarında kullanmak için sonra silinebilir
                        )
                   {                    

                            if(element.part.internalComment !== null && element.part.internalComment !== "null" && element.part.internalComment !== '' && element.part.internalComment !== "")
                            {
                                const tmp= element.part.internalComment;
                                const text = tmp.replace('+','-').replace('=','').replace('[','').replace(']',''); 
                                let {logolink,producer,producerId,imageuri,description} = JSON.parse(text);
                                tmpProducer=producer;
                                tmpProducerID=producerId;
                            }  
                           if (search !== "_") {
                               if (search !== tmpProducerID) {return;}
                           }

                            PosId = element.id; 
                            PosNorder = '';
                            PosItemCode = element.part.id;
                            PosItemName = element.part.name;
                            PosX = {PosId,PosItemCode,PosItemName,PosNorder};
                            list.push(PosX);
                            //if (Posx.length>0) {son_element.splice(0,Posx.length);}                                

                        if (ItemCode === element.part.id) 
                        {   
                       
                            Name=element.part.name;
                            PartNumber=element.part.partNumber;
                            Price=element.part.pricePurchase;
                            Tax=element.part.taxRate;
                            ItemCode=element.part.id;
                            demandId=element.order.id;
                            Qty= parseInt(Qty,10) +parseInt(element.quantity,10);
                            Pos+=','+element.id+'-'+ProducerID;

                            console.log(element.part.name, element.quantity,Qty,element.order.addressName,element.order.id,Pos);
                        }
                        else if(ItemCode ==="") 
                        {   
                            ProducerID=tmpProducerID;
                            Producer=tmpProducer;                       
                            Name=element.part.name;
                            PartNumber=element.part.partNumber;
                            Price=element.part.pricePurchase;
                            Tax=element.part.taxRate;
                            ItemCode=element.part.id;
                            demandId=element.order.id;
                            Qty=parseInt(element.quantity,10);
                            Pos=element.id+'-'+ProducerID;
                            console.log(element.part.name, element.quantity,Qty,element.order.addressName,element.order.id,Pos);
                        }
                        else 
                        {      
/*                                if(tmpProducerID !== ProducerID) { // cari değişmişse
                                  console.log(ProducerID,orderType);
                                  const id =  newOrder(element.order.orderType,ProducerID);
                                  console.log(id);
                                  tmpProducerID=ProducerID;
                                } */ 
                                order ={ProducerID,Producer,ItemCode,PartNumber,Name,Qty,Price,Tax,demandId,Pos};
                                result.push(order);


                            ProducerID=tmpProducerID;
                            Producer=tmpProducer;
                            Name=element.part.name;
                            PartNumber=element.part.partNumber;
                            Price=element.part.pricePurchase;
                            Tax=element.part.taxRate;
                            ItemCode=element.part.id;
                            demandId=element.order.id;
                            Qty=parseInt(element.quantity,10);
                            Pos=element.id+'-'+ProducerID;
                            console.log(element.part.name, element.quantity,Qty,element.order.addressName,element.order.id,Pos);
                    }
                    }

                });
                   if(ItemCode !=="") //son element için
                   {
                     ProducerID=tmpProducerID;
                     Producer=tmpProducer;
                     order ={ProducerID,Producer,ItemCode,PartNumber,Name,Qty,Price,Tax,demandId,Pos};
                     result.push(order);
                    }           
//                result.sort(dynamicSort("ProducerID"));
                total.push(result);
                total.push(list);
                res.json(total);
            }); 
        });
    }
    catch(e) {
        console.log('item',e);
        res.json(result);
    }
};

exports.putPart = function(req, res, next) {
    const { id,partNumber, name ,text, internalComment } = req.params;

    try {

        var options = {
            method: 'PUT',
            url: `${domain}/api/v1/Part/`+id,
            qs: {token: token},
            headers: {'accept': 'application/xml','content-type': 'multipart/form-data; boundary=---011000010111000001101001'},
            formData: {
                'partNumber':partNumber,
                'name':name,                
                'text':text,                
                'internalComment':internalComment
            }
        };
        console.log(options);

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};

exports.partBook = function(req, res, next) {
    const {id,name, amount,date,objectId,objectName,supplierId,text} = req.params;
    try {

        var options = {
            method: 'PUT',
            url: `${domain}/api/v1/Part/`+id+'/book?',
            qs: {token: token},
            headers: {'accept': 'application/xml','content-type': 'multipart/form-data; boundary=---011000010111000001101001'},
            formData: {
                'name': name+' '+ text,
                 amount,
                 date,
/*                'part[id]':id,
                'part[objectName]':'Part',  */             
                'supplier[id]': supplierId,
                'supplier[objectName]':'Contact',
            }

        };
        console.log(options);

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};



exports.XitemCategory = function(req, res, next) {
    const { searchCategory } = req.params;
    var result = [];

            const url = `${domain}/api/v1/Part?limit=20000&offset=0&embed=category&token=${token}`;
    try {
 
             https.get(url, 
            function (response) {
            var buffer = "";
        
            response.on("data", function (chunk) {
                buffer += chunk;
            }); 
        
            response.on("end", function (err) {
                const data = JSON.parse(buffer);
                  
/*                if (searchCategory == 'Standard') { result.push(data.objects); }
                else
                { */
                        data['objects'].forEach(element => {
                           const { id, name, partNumber,taxRate, priceGross, category, text, internalComment } = element;
                           var tmp='';

                         if(element.internalComment !== null && element.internalComment !== "null" && element.internalComment !== '' && element.internalComment !== "")
                         {                   
                           const inComText = internalComment.replace('+','-').replace('=','').replace('[','').replace(']',''); 
                            let {barcode,logolink,producer,producerId,imageuri,description} = JSON.parse(inComText);
                            var image_url=imageuri.replaceAll('_','/').replaceAll('¬','_');
                            imageuri=image_url;

                                  tmp = {category, id, name, partNumber, taxRate, priceGross, text, internalComment, barcode,logolink,producer,producerId,imageuri,description};
                         }
                         else { tmp = {category, id, name, partNumber, taxRate, priceGross, text, internalComment};}
                        
                            
                            if (element.stockEnabled == '1' && tmp.imageuri )
                            {
                                
                                if (searchCategory && tmp.imageuri.indexOf('edeka.de') >0) {
                                    if (searchCategory == 'Standard' && element.partNumber == element.name) {result.push(tmp);}
                                    else if (searchCategory == category.name) {
                                        result.push(tmp);
                                    } 
                                }
                            }
                        });
                // }
                res.json(result);
            }); 
        });
    }
    catch(e) {
        console.log('item',e);
        res.json(result);
    }
};

 String.prototype.replaceAll = function (stringToFind, stringToReplace) {
        if (stringToFind === stringToReplace) return this;
        var temp = this;
        var index = temp.indexOf(stringToFind);
        while (index != -1) {
            temp = temp.replace(stringToFind, stringToReplace);
            index = temp.indexOf(stringToFind);
        }
        return temp;
    };

exports.postPart = function(req, res, next) {
    const {partNumber,name,stock,taxRate,priceGross,categoryId} = req.params;
    console.log(partNumber,name,stock,taxRate,priceGross,categoryId);

 try {
        let formData = `partNumber=${partNumber}&name=${name}&stock=${stock}\
                        &taxRate=${taxRate}&unity[id]=1&unity[objectName]=Unity\
                        &priceGross=${priceGross}&category[id]=${categoryId}\
                         &category[objectName]=Category`;

        formData = formData.replace(/\s+/g, '');
        formData = formData.replace(/\_+/g, ' ');
        formData = getUrlVars(formData);

        console.log(formData);
        // last - son
        request.post({url:`${domain}/api/v1/Part?token=${token}`, form: formData}, 
            function(err,httpResponse,body){ 
                if (!err) {
                    res.json(JSON.parse(body));
                }
            }
        );

    } catch(error) {
      console.log(error);
    } 
};

