var https = require('https');
var request = require('request');
const var_dump = require('var_dump');
const { token, domain } = require('../../constant');
const { makeRequest, getDate, getUrlVars, dynamicSort } = require('../../common');

exports.customer = function(req, res, next) {
    const { search } = req.params;
    const url=`${domain}/api/v1/Contact?limit=1500&offset=0&token=${token}`; 

    https.get(url, 
        function (response) {
        var buffer = "";
    
        response.on("data", function (chunk) {
            buffer += chunk;
        }); 
    
        response.on("end", function (err) {
            const data = JSON.parse(buffer);
            
            var result = [];

            data["objects"].forEach(element => {
                const {id, name, status, customerNumber, category } = element;
                const customer = {
                    id, name, status, customerNumber
                };

                if (search !=='null' && search !=='' ) {
                    if (name.toLowerCase().indexOf(search.toLowerCase()) !== -1 ) {
                        result.push(customer);
                    }
                }
                else 
                {
                   if ( category.id == 140034 ) { // Markt kategorisinde ise
                        result.push(customer);
                    }
                }
            });
            result.sort(dynamicSort("name"));
            res.json(result);
        }); 
    });
};

exports.getAddresses = async function(req, res, next) {
    const { id } = req.params;

    try {
        const result = await makeRequest(`${domain}/api/v1/Contact/${id}/? embed=addresses,addresses.country, &token=${token}`);
        res.json(result.objects);
    } catch(error) {
      console.log(error);
    } 
};


exports.getEmail = async function(req, res, next) {
    const { id } = req.params;

    try {
        const result = await makeRequest(`${domain}/api/v1/Contact/${id}/getMainEmail?token=${token}`);
        res.json(result.objects);
    } catch(error) {
      console.log(error);
    } 
};

exports.getCustomerTagList = async function(req, res, next) {
   const { zip, categ, str } = req.params;

   try{

 /*       console.log('zip:'zip);
        console.log('categ:' categ)*/

        zip1= zip+"000", zip2=zip+"999";
 
        https.get(`${domain}/api/v1/TagRelation?limit=100000&offset=0&embed=tag,object & orderBy[4][field]=tag&orderBy[4][arrangement]=asc&orderBy[5][field]=object&orderBy[5][arrangement]=asc &token=${token}`, 
        function (response) {
        var buffer = "";
    
        response.on("data", function (chunk) {
            buffer += chunk;
        }); 
    
        response.on("end", function (err) {
            const data = JSON.parse(buffer);
            
            var result = [];

            data["objects"].forEach(element => {
                const {id, name, status, customerNumber,category,addresses } = element;
                const customer = {
                    id, name, status, customerNumber,category,addresses
                };


                if (zip !== 'null' && categ !== 'null') {
       //             if (category.name == search && addresses[0].zip != null ) 
                 if (category.id == categ && addresses[0].zip >= zip1 &&  addresses[0].zip <= zip2 ) 
                   {
                        result.push(customer);
                   }
                }


            });
            result.sort(dynamicSort("name"));
            res.json(result);
        }); 
        });

       } catch(error) {
      console.log(error);
    }
};


exports.getCustomerCategory = async function(req, res, next) {
   const { zip, categ, str } = req.params;

   try{

 /*       console.log('zip:'zip);
        console.log('categ:' categ)*/

        zip1= zip+"00000", zip2=zip+"99999";
 
        https.get(`${domain}/api/v1/Contact?depth=0&limit=1500&offset=0&embed=category,addresses&token=${token}`, 
        function (response) {
        var buffer = "";
    
        response.on("data", function (chunk) {
            buffer += chunk;
        }); 
    
        response.on("end", function (err) {
            const data = JSON.parse(buffer);
            
            var result = [];

            data["objects"].forEach(element => {
                const {id, name, status, customerNumber,category,addresses } = element;
                const customer = {
                    id, name, status, customerNumber,category,addresses
                };


                if (zip !== 'null' && categ !== 'null') {
       //             if (category.name == search && addresses[0].zip != null ) 
                 if (category.id == categ && addresses[0].zip >= zip1 &&  addresses[0].zip <= zip2 ) 
                   {
                        result.push(customer);
                   }
                }
                else if (str  !== 'null') {
                    if (name.toLowerCase().indexOf(str.toLowerCase()) !== -1 )
                     {
                        result.push(customer);
                    }
                }


            });
            result.sort(dynamicSort("name"));
            res.json(result);
        }); 
        });

       } catch(error) {
      console.log(error);
    }
};

exports.getCustomerLocation = async function(req, res, next) {
   const { street, zip, country } = req.params;

   try{
        const url = `https://api.tomtom.com/search/2/geocode/${zip} ${street}.json?countrySet=${country}&key=n4R3TIlrxbJuVuUjseoYAaLi3r3vZAgU`;
 //       console.log(url);
        https.get(url, 
        function (response) {
        var buffer = "";
    
        response.on("data", function (chunk) {
            buffer += chunk;
        }); 
    
        response.on("end", function (err) {
            const data = JSON.parse(buffer);
            
            var result = [];

            // data["results"].forEach(element => {
            //     const {address, position } = element;
            //     const location = { address, position };

                
            //      if (address.postalCode == zip &&  address.streetName.toLowerCase().indexOf(street.toLowerCase()) !== -1  ) 
            //        {
            //             result.push(location);
            //        }
                

            // });

            // result.sort(dynamicSort("name"));
            result.push(data.results[0]);
            res.json(result);
        }); 
        });

       } catch(error) {
      console.log(error);
    }
};
/*exports.getPdf = async function(req, res, next) {
    const { id } = req.params;
    try {
        const result = await makeRequest(`${domain}/api/v1/Order/${id}/getPdf?preventSendBy=false&token=${token}`);
        res.json(result.objects);
    } catch(error) {
      console.log(error);
    } 
};*/

exports.getCommunicationWays = async function(req, res, next) {
    const { id } = req.params;

    try {
        const result = await makeRequest(`${domain}/api/v1/Contact/${id}/getCommunicationWays?token=${token}`);
        res.json(result.objects);
    } catch(error) {
      console.log(error);
    } 
};

exports.putCustomer = function(req, res, next) {
    const { id, text } = req.params;
     console.log(id , text);
    try {
        let formData = `description=${text}`;

        formData = formData.replace(/\s+/g, '');
        formData = formData.replace(/\_+/g, ' ');
        formData = getUrlVars(formData);


        console.log(id, formData);

        // last - son
        request.post({url:`${domain}/api/v1/Contact/${id}?token=${token}`, form: formData}, 
            function(err,httpResponse,body){ 
                if (!err) {
                    res.json(JSON.parse(body));
                }
            }
        );
    } catch(error) {
      console.log(error);
    } 
};

