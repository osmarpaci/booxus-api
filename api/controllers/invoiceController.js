var https = require('https');
var request = require('request');
const var_dump = require('var_dump');
const { token, domain } = require('../../constant');
const { makeRequest, getDate, getUrlVars, dynamicSort } = require('../../common');
const { parts} = require('../../parts');

exports.invoice = function(req, res, next) {
    const { id, search } = req.params;
    const customerid = id;

    try {
         const url = `${domain}/api/v1/Invoice?limit=10000&offset=0&token=${token}`;
//         console.log(url);
        https.get(url, 
            function (response) {
            var buffer = "";
        
            response.on("data", function (chunk) {
                buffer += chunk;
            }); 
        
            response.on("end", function (err) {
                const data = JSON.parse(buffer);
                
                var result = [];
                data['objects'].forEach(element => {
                    const {id, header, address, invoiceNumber,contact, sumGross,  sumNet, currency, invoiceDate } = element;
                    if (contact.id !== customerid) return;
                    const invoice = {
                        id, header, address, invoiceNumber, sumGross, sumNet, currency, invoiceDate
                    };

                    if (search) {
                        if (header.toLowerCase().indexOf(search.toLowerCase()) !== -1) {
//                            result.push(invoice);
                            result.push(element);

                        } 
                        else 
                        if (invoiceNumber.toLowerCase().indexOf(search.toLowerCase()) !== -1) {
//                            result.push(invoice);
                            result.push(element);
                        } 
                    }
                    else 
                    {
//                            result.push(invoice);
                            result.push(element);
                    }
                });
                res.json(result);
            }); 
        });
    }
    catch(e) {
        console.log('invoice',e);
    }
};

exports.getPositions = function(req, res, next) {
    const { id } = req.params;
    var result = [];
    try {
        https.get(`${domain}/api/v1/Invoice/${id}/getPositions/?limit=10000&offset=0&token=${token}`, 
            function (response) {
            var buffer = "";
        
            response.on("data", function (chunk) {
                buffer += chunk;
            }); 
        
            response.on("end", function (err) {
   
                const data = JSON.parse(buffer);
                if (data['objects']===null) {
                    res.json(result);
                }

                data['objects'].forEach(element => {
                    const {id, name, quantity, price, taxRate, sumNet, sumGross} = element;
                    const invoice = {
                        id, name, quantity, price, taxRate, sumNet, sumGross
                    };
                    result.push(invoice);
                });
                res.json(result);
            }); 
        });
    }
    catch(e) {
        console.log('getPosition',e);
    }
};

exports.getPositionsExtended = function(req, res, next) {
    const { id } = req.params;
    var result = [];

    try {
        https.get(`${domain}/api/v1/Invoice/${id}/getPositions/?limit=10000&offset=0&token=${token}`, 
            function (response) {
            var buffer = "";
        
            response.on("data", function (chunk) {
                buffer += chunk;
            }); 
        
            response.on("end", function (err) {
   
                const data = JSON.parse(buffer);
                if (data['objects']===null) {
                    res.json(result);
                }

                data['objects'].forEach(element => {
                    const {id, name, quantity, price, taxRate, sumNet, sumGross} = element;
                    const invoice = {
                        id, name, quantity, price, taxRate, sumNet, sumGross
                    };
                    var resultExt=[];
                    try{
                        parts.forEach(partExt => { 
                        if (partExt.id) {
                            if (element.part.id === partExt.id) {
                            resultExt.push(element);
                            resultExt.push(partExt);
                            result.push(resultExt);
                            } 
                        }
                        });
                    } catch(e) {}
                });
                res.json(result);
            }); 
        });
    }
    catch(e) {
        console.log('getPosition',e);
    }


};
exports.getNextInvoiceNumber = async function(req, res, next) {
    const { invoiceType } = req.params;
    
    try {
        const result = await makeRequest(`${domain}/api/v1/Invoice/Factory/getNextInvoiceNumber?invoiceType=${invoiceType}&useNextNumber=true&token=${token}`);
        res.json(result.objects);
    } catch(error) {
      console.log(error);
    } 
};

exports.postInvoice = async function(req, res, next) {
    const { invoiceType, id } = req.params;

    try {
        const invoiceDate = getDate();
        const invoiceNo = await makeRequest(`${domain}/api/v1/Invoice/Factory/getNextInvoiceNumber?invoiceType=${invoiceType}&useNextNumber=true&token=${token}`);
        const invoiceNumber = invoiceNo.objects;

        const address = await makeRequest(`${domain}/api/v1/Contact/${id}/getAddresses?limit=10000&offset=0&token=${token}`);
        
        // ilk adres satırını al sonra terminalde seçsin.
        const header = `Invoice_Confirmation_${invoiceNumber}`; 
        let contact_id = null;
        let addressStreet = '';
        let addressZip = '';
        let addressCity = '';
        if (address.objects.length>0) {
            //var_dump(address.objects[0]);
            contact_id = address.objects[0].id;
            addressStreet = address.objects[0].street;
            addressZip = address.objects[0].zip;
            addressCity = address.objects[0].city;
        }

        // eski desen
/*        let formData = `invoiceNumber=${invoiceNo.objects}&invoiceType=${invoiceType}&contact[id]=${id}&contact[objectName]=Contact&invoiceDate=${invoiceDate}&\
        header=${header}&headText=null&footText=null&addressName=Factory&addressStreet=${addressStreet}&addressZip=${addressZip}&addressCity=${addressCity}&\
        smallSettlement=0&contactPerson[id]=321700&contactPerson[objectName]=SevUser&\
        taxRate=0&taxText=Umsatzsteuer ausweisen&taxType=default&currency=EUR&version=1`;
        formData = formData.replace(/\s+/g, '');
        formData = formData.replace(/\_+/g, ' ');
        formData = getUrlVars(formData);*/


        // desen
        let formData = `header=${header}&invoiceNumber=${invoiceNo.objects}&invoiceType=${invoiceType}&invoiceDate=${invoiceDate}&\
        contactPerson[id]=321700&contactPerson[objectName]=SevUser&contact[id]=${id}&contact[objectName]=Contact&\
        discount=0&discountTime=&taxRate=0&taxText=Umsatzsteuer ausweisen&taxType=default&\
        status=100&smallSettlement=0&currency=EUR`;

        formData = formData.replace(/\s+/g, '');
        formData = formData.replace(/\_+/g, ' ');
        formData = getUrlVars(formData);


//        console.log(formData);

        // last - son
        request.post({url:`${domain}/api/v1/Invoice?token=${token}`, form: formData}, 
            function(err,httpResponse,body){ 
                if (!err) {
                    res.json(JSON.parse(body));
                }
            }
        );
    } catch(error) {
      console.log(error);
    } 
};

exports.postInvoiceMaster = async function(req, res, next) {
    const { invoiceType, id,name,addressStreet, addressZip, addressCity  } = req.params;

    try {
        const invoiceDate = getDate();
        const urli= `${domain}/api/v1/Invoice/Factory/getNextInvoiceNumber?invoiceType=${invoiceType}&useNextNumber=true&token=${token}`;
        console.log(urli); 
        const invoiceNo = await makeRequest(urli);
        const invoiceNumber = invoiceNo.objects;
        console.log(invoiceNumber);

        //const address = await makeRequest(`${domain}/api/v1/Contact/${id}/getAddresses?limit=10000&offset=0&token=${token}`);
        
        // ilk adres satırını al sonra terminalde seçsin.
        const header = `Invoice_Confirmation_${invoiceNumber}`; 
/*        let contact_id = null;
        let addressStreet = '';
        let addressZip = '';
        let addressCity = '';

        if (address.objects.length>0) {
            //var_dump(address.objects[0]);
            contact_id = address.objects[0].id;
            addressStreet = address.objects[0].street;
            addressZip = address.objects[0].zip;
            addressCity = address.objects[0].city;
        }*/

        
/*        let formData = `invoiceNumber=${invoiceNo.objects}&invoiceType=${invoiceType}&contact[id]=${21361764}&contact[objectName]=Contact&invoiceDate=${invoiceDate}&\
        header=${header}&headText=null&footText=null&\
        addressName=${name}&addressStreet=${addressStreet}&addressZip=${addressZip}&addressCity=${addressCity}&\
        smallSettlement=0&contactPerson[id]=415729&contactPerson[objectName]=SevUser&\
        taxRate=0&taxText=Umsatzsteuer ausweisen&taxType=default&currency=EUR&version=1`;*/

        let formData = `header=${header}&invoiceNumber=${invoiceNo.objects}&invoiceType=${invoiceType}&invoiceDate=${invoiceDate}&\
        addressName=${name}&addressStreet=${addressStreet}&addressZip=${addressZip}&addressCity=${addressCity}&\
        contactPerson[id]=415729&contactPerson[objectName]=SevUser&contact[id]=${21361764}&contact[objectName]=Contact&\
        discount=0&discountTime=&taxRate=0&taxText=Umsatzsteuer ausweisen&taxType=default&\
        status=100&smallSettlement=0&currency=EUR`;

        formData = formData.replace(/\s+/g, '');
        formData = formData.replace(/\_+/g, ' ');
        formData = getUrlVars(formData);

//        console.log(formData);

        // last - son
        request.post({url:`${domain}/api/v1/Invoice?token=${token}`, form: formData}, 
            function(err,httpResponse,body){ 
                if (!err) {
                    res.json(JSON.parse(body));
                }
            }
        );
    } catch(error) {
      console.log(error);
    } 
};

exports.setInvoicePos = function(req, res, next) {
    const { invoiceid, quantity, price, tax, name } = req.params;
    
    try {

        var options = {
            method: 'POST',
            url: `${domain}/api/v1/InvoicePos`,
            qs: {token: token},
            headers: {'content-type': 'multipart/form-data; boundary=---011000010111000001101001'},
            formData: {
                'invoice[id]': invoiceid,
                'invoice[objectName]': 'Invoice',
                quantity,
                price,
                taxRate: tax,
                'unity[id]': '1',
                'unity[objectName]': 'Unity',
                name
            }
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};

exports.putInvoicePos = function(req, res, next) {
    const { id, invoiceid, quantity, price, tax, name } = req.params;
    
    try {

        var options = {
            method: 'PUT',
            url: `${domain}/api/v1/InvoicePos/`+id,
            qs: {token: token},
            headers: {'content-type': 'multipart/form-data; boundary=---011000010111000001101001'},
            formData: {
                'invoice[id]': invoiceid,
                'invoice[objectName]': 'Invoice',
                quantity,
                price,
                taxRate: tax,
                'unity[id]': '1',
                'unity[objectName]': 'Unity',
                name
            }
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};

exports.putPositionText = async function(req, res, next) {
    const { id, partId } = req.params;
    
    try {

         const lastLog = await makeRequest(`${domain}/api/v1/Part/${partId}/getLastLog?&token=${token}`);
         const text = lastLog.objects.id;
         console.log(text);

        var options = {
            method: 'PUT',
            url: `${domain}/api/v1/InvoicePos/`+id,
            qs: {token: token},
            headers: {'content-type': 'multipart/form-data; boundary=---011000010111000001101001'},
            formData: {
                text
            }
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};

exports.rmInvoicePos = function(req, res, next) {
    const { id } = req.params;
    
    try {

        console.log('Fatura kalemi silme islemi', id);
        var options = {
            method: 'DELETE',
            url: `${domain}/api/v1/InvoicePos/`+id,
            qs: {token: token},
            headers: {'content-type': 'application/json'}
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};

exports.rmInvoice = function(req, res, next) {
    const { id } = req.params;
    
    try {

        console.log('Fatura üstkayıt silme islemi', id);
        var options = {
            method: 'DELETE',
            url: `${domain}/api/v1/Invoice/`+id,
            qs: {token: token},
            headers: {'content-type': 'application/json'}
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};

exports.sendInvoiceEmail = async function(req, res, next) {
    const { email, id } = req.params;

    try {
   
        // desen
        let formData = `toEmail=${email}&subject=Enfess den gönderilen mail`;

        formData = formData.replace(/\s+/g, '');
        formData = formData.replace(/\_+/g, ' ');
        formData = getUrlVars(formData);


        console.log(formData);

        // last - son
        request.post({url:`${domain}api/v1/Invoice/${id}/sendViaEmail?token=${token}`, form: formData}, 
            function(err,httpResponse,body){ 
                if (!err) {
                    res.json(JSON.parse(body));
                }
            }
        );
    } catch(error) {
      console.log(error);
    } 
};
