
var https = require('https');
var request = require('request');
const var_dump = require('var_dump');
const { token, domain } = require('../../constant');
const { makeRequest, getDate, getUrlVars, dynamicSort, getDetails, Sleep} = require('../../common');


exports.getTagList = async function(req, res, next) {
   const { userTag, tagType } = req.params;

   try{

        const url = `${domain}/api/v1/TagRelation?limit=10000&offset=0&embed=tag,object&token=${token}`;
        https.get(url, 
        function (response) {
        var buffer = "";
    
        response.on("data", function (chunk) {
            buffer += chunk;
        }); 

        response.on("end", function (err) {
            const data = JSON.parse(buffer);
            
            var result = [];
           if(data)
           {
              data["objects"].forEach(element => {
                  const {tag,object } = element;
                  const liste = { tag,object};
                  if (userTag !== 'null' && tagType !== 'null') {
                   if (userTag == tag.name && tagType == object.objectName ) 
                     {result.push(liste);}
                  }
                  else
                  {  result.push(liste);}
              });
            }
            res.json(result);
        }); 
        });




       } catch(error) {
      console.log(error);
    }

};


exports.getTagCariList = async function(req, res, next) {
   const { userTag, search } = req.params;

   try{

        const url = `${domain}/api/v1/TagRelation?limit=10000&offset=0&embed=tag,object&token=${token}`;

        https.get(url, 
        function (response) {
        var buffer = "";
    
        response.on("data", function (chunk) {
            buffer += chunk;
        }); 

        response.on("end", function (err) {
            const data = JSON.parse(buffer);
            
            var result = [];
          
            data["objects"].forEach(element => {
                const {tag,object } = element;
                const liste = { tag,object};
             if (object.objectName == 'Contact') {
                 if (userTag == tag.name) 
                   {

                        if (search !=='null') {
                            if (object.name !== null)
                            {
                                if (object.name.toLowerCase().indexOf(search.toLowerCase()) !== -1 ) {
                                    result.push(object);
                                }
                            }
                        }
                        else
                        {result.push(object);}
                   }
                 
              }
            });
            res.json(result);
        }); 
        });




       } catch(error) {
      console.log(error);
    }

};

exports.postTagRelation = async function(req, res, next) {
   const { userTagId,tagObjectName, objectId,objectName } = req.params;
   try{

         let tag_present=0;
        let formData = `tag[id]=${userTagId}&tag[objectName]=${tagObjectName}&object[id]=${objectId}&object[objectName]=${objectName}`;
        formData = formData.replace(/\s+/g, '');
        formData = formData.replace(/\_+/g, ' ');
        formData = getUrlVars(formData);

        console.log(formData);

            request.post({url:`${domain}/api/v1/TagRelation?token=${token}`, form: formData}, 
            function(err,httpResponse,body){ 
                if (!err) {
                    res.json(JSON.parse(body));
                }
            }
        );

       } catch(error) {
      console.log(error);
    }

};



// duzenlenecek
exports.getTagDetailList = async function(req, res, next) {
   const { userTag, tagType } = req.params;

   try{
        const url = `${domain}/api/v1/TagRelation?limit=10000&offset=0&embed=tag,object&token=${token}`;
        let sonuc1 = await makeRequest(url);
        var result = [];

        sonuc1["objects"].forEach(async (element) => {
            const {tag,object } = element;
            const liste = { tag,object};
                        
            if (userTag !== 'null' && tagType !== 'null') {
              if (userTag === tag.name && tagType === object.objectName ) {
//              console.log(tag.name, ':', userTag ,'-', object.objectName, ':',tagType, ':',object.id);
                const urlx=`${domain}/api/v1/Invoice/${object.id}/getPositions/?limit=1000&offset=0&token=${token}`;

/*              let detay = getDetails(urlx);
                console.log(detay);
                result.push(detay);*/
                console.log(urlx);
                
                let sonuc2 = await makeRequest(urlx);
          
                  sonuc2["objects"].forEach(async (detay) => {
                   result.push(detay);
//                    console.log(detay);
                  });  
              }
            }
          });
          console.log('bitti');
          res.json(await result);

       } catch(error) {
      console.log(error);
    }

};

