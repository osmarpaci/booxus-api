const { users } = require('../../constant');

exports.user = function(req, res, next) {
    const { email, password } = req.params;
    let result = [];
    let durum = false;

    try {

        users.forEach(function(element){
            if (element.tag === email || element.email === email)
            {
                if (element.pass === password) {
                    durum = true;
                    result.push({status: true, ...element});  
                }
            } 
        });

        if (!durum) {
            result.push({status: false}); 
        }
        res.json(result); 
    }
    catch(e) {
        console.log('user',e);
    }
};