var fs = require('fs'), request = require('request');
const { exec } = require('child_process');

var download = function(uri, filename, callback){
  request.head(uri, function(err, res, body){
    console.log('content-type:', res.headers['content-type']);
    console.log('content-length:', res.headers['content-length']);

    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};

exports.index =  function(req, res, next) {
  var url = req.body.url;
  var name = req.body.name;
  var resulation = req.body.resulation;
  var cmd = "";
  try {
    console.log("url:",url);
    console.log("name:",name);
    console.log("resulation:",resulation);
    var dir = 'C:\\inetpub\\wwwroot\\booxus\\public\\Download\\';

    download(
      url, 
      `${dir}${name}`, function(){
        console.log('done');

        // convert s.jpg -size 800x800 xc:white +swap -gravity center -composite n1.jpg
        cmd = `magick ${dir}${name} -size ${resulation} xc:white +swap -gravity center -composite ${dir}${name}`;
        console.log("cmd:",cmd);
        exec(cmd, 
          (err, stdout, stderr) => {
            if (err) {
              // node couldn't execute the command
              console.log(err);
              return;
            }
        });
        res.json("{'status':'ok', 'url':'http://81.169.209.191:8090/download/"+name+"'}");
    });
  }
  catch(e){
    res.json("{'status':'error','msg':'"+e+"'}"); 
  } 
};
