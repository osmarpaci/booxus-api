var https = require('https');
const { token, domain } = require('../../constant');
const { parts} = require('../../parts');
const { edeka} = require('../../edeka');

exports.YgetCategory = async function(req, res, next) {
    const { search } = req.params;
            
            var result = [];
            var tmpCategory='';

            parts.forEach(element => {
                const {subCategory } = element;
               
                if (tmpCategory !== subCategory) {
                        var name = subCategory;
                        var id= "1";
                        result.push(name,id);
                        tmpCategory = subCategory;
                }

            });
            res.json(result);
        
};

exports.getCategory = async function(req, res, next) {
    const { search } = req.params;

       if (search == 'Edeka') 
       {
            var result = [];
            var tmpCategory='';

            edeka.forEach(element => {
                const {subCategory } = element;
               
                if (tmpCategory !== subCategory) {
                        var name = subCategory;
                        var id= "1";
                        const tmp={name,id};
                        result.push(tmp);
                        tmpCategory = subCategory;
                }

            });
            res.json(result);
       }
       else
       {
                   https.get(`${domain}/api/v1/Category?limit=250&offset=0&token=${token}`, 
                    function (response) {
                    var buffer = "";
                
                    response.on("data", function (chunk) {
                        buffer += chunk;
                    }); 
                
                    response.on("end", function (err) {
                        const data = JSON.parse(buffer);
                        
                        var result = [];

                        data["objects"].forEach(element => {
                            const {objectType } = element;
                           
                            if (search) {
                                if (search == objectType ) {
                                    result.push(element);
                                }
                            }
                            else 
                            {
                                    result.push(element);
                              
                            }
                        });
                        res.json(result);
                    }); 
                  });
        }
};


exports.getOptimizedRoute = async function(req, res, next) {
   const { origin, waypoints, destination } = req.params;
//   console.log(origin,waypoints,destination)
   try{
        const url = `https://api.tomtom.com/routing/1/calculateRoute/${origin}:${waypoints}:${destination}/json?computeBestOrder=true&routeType=fastest&key=n4R3TIlrxbJuVuUjseoYAaLi3r3vZAgU`;
//        console.log(url);

        https.get(url, 
        function (response) {
        var buffer = "";
    
        response.on("data", function (chunk) {
            buffer += chunk;
        }); 
    
        response.on("end", function (err) {
            const data = JSON.parse(buffer);
   
            res.json(data);
        }); 
        });

       } catch(error) {
      console.log(error);
    }
};


exports.getPoi = async function(req, res, next) {
   const { search, lat, lon, radius } = req.params;
//   console.log(origin,waypoints,destination)
   try{
//        const urlx = `https://api.tomtom.com/routing/1/calculateRoute/${origin}:${waypoints}:${destination}/json?computeBestOrder=true&routeType=fastest&key=n4R3TIlrxbJuVuUjseoYAaLi3r3vZAgU`;
        const url  = `https://api.tomtom.com/search/2/search/${search}.json?lat=${lat}&lon=${lon}&radius=${radius}&limit=1000&countrySet=DE&key=n4R3TIlrxbJuVuUjseoYAaLi3r3vZAgU`;
//        console.log(url);

        https.get(url, 
        function (response) {
        var buffer = "";
    
        response.on("data", function (chunk) {
            buffer += chunk;
        }); 
    
        response.on("end", function (err) {
            const data = JSON.parse(buffer);
   
            res.json(data.results);
        }); 
        });

       } catch(error) {
      console.log(error);
    }
};

exports.xbase64 = async function(req, res, next) {
   const { search} = req.params;
   console.log(search);
   try
    {
            const {decode} = require('js-base64');
            const data = decode(search);
            console.log(data);
            res.json(data);
    }
    catch(error) {
      console.log(error);
    }
};

