var https = require('https');
var request = require('request');
const var_dump = require('var_dump');
const { token, domain } = require('../../constant');
const { makeRequest, getDate, getUrlVars, dynamicSort } = require('../../common');

exports.order = function(req, res, next) {
    const { id, search } = req.params;
    const customerid = id;
    let limit=10000; 


    try {
        if (customerid=='_') limit = 30; 
        const url = `${domain}/api/v1/Order?limit=${limit}&offset=0&token=${token}`;
        https.get(url, 
            function (response) {
            var buffer = "";
        
            response.on("data", function (chunk) {
                buffer += chunk;
            }); 
        
            response.on("end", function (err) {
                const data = JSON.parse(buffer);

                var result = [];

                data['objects'].forEach(element => {
                    const {id, status, header, address, orderNumber, contact, sumNet,sumTax, sumGross, currency, orderDate, sendType, orderType } = element;
                     const order = { id, status, header, address, orderNumber, sumNet, sumTax,sumGross, currency, orderDate, sendType
                    };

                    if (customerid !== "_" )
                    {
                      if( contact.id !== customerid) return;
                    }

                    if (search == "AB" || search == "AN")
                    {
                       if (search == orderType) {result.push(order)}
                        return;
                    }

                    if (search) {
                        if (header.toLowerCase().indexOf(search.toLowerCase()) !== -1) {
                            result.push(order);
                        } 
                        else 
                        if (orderNumber.toLowerCase().indexOf(search.toLowerCase()) !== -1) {
                            result.push(order);
                        } 
                    }
                    else 
                    {
                        result.push(order);
                    }
                });
                //result.sort(dynamicSort("-orderNumber"));
                res.json(result);
            }); 
        });
    }
    catch(e) {
        console.log('costomer',e);
    }
};

exports.getOrder = function(req, res, next) {
    const { id,type,stat, search } = req.params;
    const customerid = id;
    console.log(customerid);
    try {
        const url = `${domain}/api/v1/Order?limit=10000&offset=0&token=${token}`;
        https.get(url, 
            function (response) {
            var buffer = "";
        
            response.on("data", function (chunk) {
                buffer += chunk;
            }); 
        
            response.on("end", function (err) {
                const data = JSON.parse(buffer);

                var result = [];

                data['objects'].forEach(element => {
                    const {id, status, header, address, orderNumber, contact, sumNet,sumTax, sumGross, currency, orderDate, sendType, orderType, addressName, addressStreet, addressZip, addressCity } = element;
    
                    const order = {
                        id, contact, status, header, address, orderNumber, sumNet,sumTax,sumGross, currency, orderDate, sendType, addressName, addressStreet, addressZip, addressCity
                    };

//                    if (orderDate < '2020-02-13T00:00:00+01:00') {return;}
                    if (customerid !== "" && customerid !== null && customerid !== "_")
                    {
                       if (contact.id !== customerid) {return;}
                    }
                    if (type !== "" && type !== null && type !== "_")
                    {
                       if (type !== orderType) {return;}
                    }
                    if (stat !== "" && stat !== null && stat !== "_")
                    {
                       if (stat !== status) {return;}
                    }

                    if (search !== "_") {
                        if (addressName.toLowerCase().indexOf(search.toLowerCase()) !== -1) {
                            result.push(order);
                        } 
                        else 
                        if (orderNumber.toLowerCase().indexOf(search.toLowerCase()) !== -1) {
                            result.push(order);
                        } 
                    }
                    else 
                    {
                        result.push(order);
                    }
                });
                //result.sort(dynamicSort("-orderNumber"));
                res.json(result);
            }); 
        });
    }
    catch(e) {
        console.log('costomer',e);
    }
};

exports.getOrderPositions = function(req, res, next) {
    const { id } = req.params;
    var result = [];

    console.log(id);

    try {
        const url = `${domain}/api/v1/Order/${id}/getPositions/?limit=100&offset=0 &embed=part,order&token=${token}`; 

        https.get(url, 
            function (response) {
            var buffer = "";
        
            response.on("data", function (chunk) {
                buffer += chunk;
            }); 
        
            response.on("end", function (err) {
                const data = JSON.parse(buffer);
                if (data['objects']===null) {
                    res.json(result);
                }
             if (data)
            {
                data['objects'].forEach(element => {
                    const {id, name, quantity, price, taxRate, sumGross, sumNet} = element;
                    const order = {
                        id, name, quantity, price, taxRate, sumGross, sumNet
                    };
                    result.push(element);
                });
            }                
                res.json(result);
            }); 
        });
    }
    catch(e) {
        console.log('getPosition',e);
        res.json(result);
    }
};

exports.getNextOrderNumber = async function(req, res, next) {
    const { orderType } = req.params;
    
    try {
        const result = await makeRequest(`${domain}/api/v1/Order/Factory/getNextOrderNumber?orderType=${orderType}&useNextNumber=true&token=${token}`);
        res.json(result.objects);
    } catch(error) {
      console.log(error);
    } 
};

exports.postOrder = async function(req, res, next) {
    const { orderType,name,id } = req.params;
    console.log(orderType, name,id);

    try {
        const orderDate = getDate();
        const orderNo = await makeRequest(`${domain}/api/v1/Order/Factory/getNextOrderNumber?orderType=${orderType}&useNextNumber=true&token=${token}`);
        const orderNumber = orderNo.objects;
        console.log(orderNo);
        const address = await makeRequest(`${domain}/api/v1/Contact/${id}/getAddresses?limit=10000&offset=0&token=${token}`);
        
        // ilk adres satırını al sonra terminalde seçsin.
        const header = `Order_Confirmation_${orderNumber}`; 
        let contact_id = null;
        let addressStreet = '';
        let addressZip = '';
        let addressCity = '';

        // desen
        let formData = `orderNumber=${orderNo.objects}&orderType=${orderType}&contact[id]=${21361764}&contact[objectName]=Contact&orderDate=${orderDate}&\
        header=${header}&headText=null&footText=null&addressName=${name}&addressStreet=${addressStreet}&addressZip=${addressZip}&addressCity=${addressCity}&\
        smallSettlement=0&contactPerson[id]=415729&contactPerson[objectName]=SevUser&\
        showNet=0&taxRate=0&taxText=Umsatzsteuer ausweisen&taxType=default&currency=EUR&version=1`;
        formData = formData.replace(/\s+/g, '');
        formData = formData.replace(/\_+/g, ' ');
        formData = getUrlVars(formData);



        // last - son
        request.post({url:`${domain}/api/v1/Order?token=${token}`, form: formData}, 
            function(err,httpResponse,body){ 
                if (!err) {
                    res.json(JSON.parse(body));
                }
            }
        );
    } catch(error) {
      console.log(error);
    } 
};

exports.postOrderMaster = async function(req, res, next) {
    const { orderType,id,wooId,name,addressStreet, addressZip, addressCity  } = req.params;
    console.log(orderType,id,wooId,name,addressStreet, addressZip, addressCity);

    try {
        const orderDate = getDate();
        const orderNo = await makeRequest(`${domain}/api/v1/Order/Factory/getNextOrderNumber?orderType=${orderType}&useNextNumber=true&token=${token}`);
        const orderNumber = orderNo.objects;
        console.log(orderNo);
        const address = await makeRequest(`${domain}/api/v1/Contact/${id}/getAddresses?limit=10000&offset=0&token=${token}`);
        
        // ilk adres satırını al sonra terminalde seçsin.
        const header = wooId; 
/*        let contact_id = null;
        let addressStreet = '';
        let addressZip = '';
        let addressCity = '';*/

        // desen
        let formData = `orderNumber=${orderNo.objects}&orderType=${orderType}&contact[id]=${21361764}&contact[objectName]=Contact&orderDate=${orderDate}&\
        header=${header}&headText=null&footText=null&addressName=${name}&addressStreet=${addressStreet}&addressZip=${addressZip}&addressCity=${addressCity}&\
        smallSettlement=0&contactPerson[id]=415729&contactPerson[objectName]=SevUser&\
        showNet=0&taxRate=0&taxText=Umsatzsteuer ausweisen&taxType=default&currency=EUR&version=1`;
        formData = formData.replace(/\s+/g, '');
        formData = formData.replace(/\_+/g, ' ');
        formData = getUrlVars(formData);



        // last - son
        request.post({url:`${domain}/api/v1/Order?token=${token}`, form: formData}, 
            function(err,httpResponse,body){ 
                if (!err) {
                    res.json(JSON.parse(body));
                }
            }
        );
    } catch(error) {
      console.log(error);
    } 
};


exports.putcustomerInternalNote = function(req, res, next) {
    const { demandId,orderId} = req.params;
    console.log(demandId,orderId);
    try {

        var options = {
            method: 'PUT',
            url: `${domain}/api/v1/Order/`+demandId,
            qs: {token: token},
            headers: {'content-type': 'multipart/form-data; boundary=---011000010111000001101001'},
            formData: {
                'customerInternalNote': orderId
            }
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};


exports.setOrderPos = function(req, res, next) {
    const { orderid, quantity, price, tax, name, partid } = req.params;
    
    try {

        var options = {
            method: 'POST',
            url: `${domain}/api/v1/OrderPos`,
            qs: {token: token},
            headers: {'content-type': 'multipart/form-data; boundary=---011000010111000001101001'},
            formData: {
                'order[id]': orderid,
                'order[objectName]': 'Order',
                'part[id]': partid,
                'part[objectName]': 'Part',                
                quantity,
                price,
                taxRate: tax,
                'unity[id]': '1',
                'unity[objectName]': 'Unity',
                name
            }
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};

exports.putOrderPos = function(req, res, next) {
    const { id, orderid, quantity, price, tax, name, partid } = req.params;
    console.log('putOrderPos', id);
    try {

        var options = {
            method: 'PUT',
            url: `${domain}/api/v1/OrderPos/`+id,
            qs: {token: token},
            headers: {'content-type': 'multipart/form-data; boundary=---011000010111000001101001'},
            formData: {
                'order[id]': orderid,
                'order[objectName]': 'Order',
                'part[id]': partid,
                'part[objectName]': 'Part',                  
                quantity,
                price,
                taxRate: tax,
                'unity[id]': '1',
                'unity[objectName]': 'Unity',
                name
            }
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};

exports.putOrderPosConn = function(req, res, next) {
    const { id, orderid } = req.params;
            console.log(id,orderid);
    try {

        var options = {
            method: 'PUT',
            url: `${domain}/api/v1/OrderPos/`+id,
            qs: {token: token},
            headers: {'content-type': 'multipart/form-data; boundary=---011000010111000001101001'},
            formData: {
                'text': orderid,
                'unity[id]': '1',
                'unity[objectName]': 'Unity'
            }
        };
        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};


exports.rmOrderPos = function(req, res, next) {
    const { id } = req.params;
    
    try {

        console.log('malzeme silme islemi', id);
        var options = {
            method: 'DELETE',
            url: `${domain}/api/v1/OrderPos/`+id,
            qs: {token: token},
            headers: {'content-type': 'application/json'}
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};

exports.rmOrder = function(req, res, next) {
    const { id } = req.params;
    const urlx = `${domain}/api/v1/Order/${id}?token=${token}`; 

    
    try {

        var options = {
            method: 'DELETE',
            url: `${domain}/api/v1/Order/`+id,
            qs: {token: token},
            headers: {'content-type': 'application/json'}
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};

exports.copyOrder = function(req, res, next) {
    const { id } = req.params;
    const urlx = `${domain}/api/v1/Order/${id}/copy?token=${token}`; 

    
    try {
        var options = {
            method: 'POST',
            url: `${domain}/api/v1/Order/`+id+`/copy?`,
            qs: {token: token},
            headers: {'content-type': 'application/json'}
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};

exports.duplicateOrder = function(req, res, next) {
    const { id } = req.params;
    const urlx = `${domain}/api/v1/Order/${id}/duplicate?token=${token}`; 

    
    try {
        var options = {
            method: 'POST',
            url: `${domain}/api/v1/Order/`+id+`/duplicate?`,
            qs: {token: token},
            headers: {'content-type': 'application/json'}
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};

exports.sendViaEmail = async function(req, res, next) {
    const { id, toEmail, subject, text } = req.params;
    
    try {

        var options = {
            method: 'POST',
            url: `${domain}/api/v1/Order/${id}/sendViaEmail`,
            qs: {token: token},
            headers: {'content-type': 'multipart/form-data; boundary=---011000010111000001101001'},
            formData: {
                'toEmail': toEmail,
                'subject': subject,
                'text': text
            }
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            res.json(JSON.parse(body));
        });

    } catch(error) {
      console.log(error);
    } 
};
