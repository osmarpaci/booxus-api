var https = require('https');
var fs = require('fs');
const { token, domain } = require('../../constant');
const { makeRequest, dynamicSort, downloadBase64File } = require('../../common');


exports.getOrderPdf = async function(req, res, next) {
    const { id} = req.params;
    const url = `${domain}/api/v1/Order/${id}/getPdf?preventSendBy=false&token=${token}`;
    //console.log(url)
    try {
        const result = await makeRequest(url);

        const fname = './public/Documents/' + result.objects.filename;
        fs.writeFile(fname, result.objects.content, {encoding: 'base64'}, 
			function(err) {
				console.log(err);
			}
		);

        res.json(result.objects);
    } catch(error) {
      console.log(error);
    } 
};



