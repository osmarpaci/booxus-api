
var https = require('https');
var request = require('request');
const var_dump = require('var_dump');
const { token, domain } = require('../../constant');
const { makeRequest, getDate, getUrlVars, dynamicSort } = require('../../common');

exports.getTaskList = async function(req, res, next) {
   const { userTask, taskType } = req.params;

   try{

        const url = `${domain}/api/v1/Task?limit=10000&&offset=0&embed=assigned,object,,object.contact,createUser&token=${token}`;
        https.get(url, 
        function (response) {
        var buffer = "";
    
        response.on("data", function (chunk) {
            buffer += chunk;
        }); 

        response.on("end", function (err) {
            const data = JSON.parse(buffer);
            
            var result = [];

            data["objects"].forEach(element => {
  
                if (userTask !== 'null' && taskType !== 'null') {   //
                 if (userTask == element.assigned.id && taskType == element.category.id) //
                   {
                        result.push(element);
                   }
                }
                else
                {
                   result.push(element);
                }


            });
            res.json(result);
        }); 
        });




       } catch(error) {
      console.log(error);
    }

};


exports.getTaskItem = async function(req, res, next) {
   const { taskId } = req.params;

   try{

        const url = `${domain}/api/v1/Task?limit=10000&&offset=0&embed=assigned,object,createUser&token=${token}`; //?orderBy[0][field]=additionalInformation&orderBy[0][arrangement]=asc&orderBy[1][field]=id&orderBy[1][arrangement]=asc
        https.get(url, 
        function (response) {
        var buffer = "";
    
        response.on("data", function (chunk) {
            buffer += chunk;
        }); 

        response.on("end", function (err) {
            const data = JSON.parse(buffer);
            
            var result = [];

            data["objects"].forEach(element => {
  
                if (taskId !== 'null' ) {
                 if (taskId == element.id) 
                   {
                        result.push(element);
                   }
                }
                else
                {
                   result.push(element);
                }


            });
            res.json(result);
        }); 
        });




       } catch(error) {
      console.log(error);
    }

};

exports.getTaskItemObject = async function(req, res, next) {
   const { taskId, objectId } = req.params;

   try{

        const url = `${domain}/api/v1/Task?limit=10000&&offset=0&embed=assigned,object,createUser&token=${token}`; //?orderBy[0][field]=additionalInformation&orderBy[0][arrangement]=asc&orderBy[1][field]=id&orderBy[1][arrangement]=asc
        https.get(url, 
        function (response) {
        var buffer = "";
    
        response.on("data", function (chunk) {
            buffer += chunk;
        }); 

        response.on("end", function (err) {
            const data = JSON.parse(buffer);
            
            var result = [];

            data["objects"].forEach(element => {
  
                if (taskId !== 'null' && taskId !== '_') {
                 if (taskId == element.id) 
                   {
                        result.push(element);
                   }
                }
                else if (objectId !== 'null' && objectId !== '_') {
                 if (objectId == element.object.id) 
                   {
                        result.push(element);
                   }
                } 
                else
                {
                   result.push(element);
                }


            });
            res.json(result);
        }); 
        });




       } catch(error) {
      console.log(error);
    }

};

exports.doneTask = function(req, res, next) {
    const { id } = req.params;
    const urlx = `${domain}/api/v1/Task/${id}?token=${token}`; 
//    console.log(urlx);
    
 try {
        let formData = `status=1000`;
        formData = formData.replace(/\s+/g, '');
        formData = formData.replace(/\_+/g, ' ');
        formData = getUrlVars(formData);

//        console.log(formData);

        // last - son
        request.post({url:`${domain}/api/v1/Task/${id}?token=${token}`, form: formData}, 
            function(err,httpResponse,body){ 
                if (!err) {
                    res.json(JSON.parse(body));
                }
            }
        );

    } catch(error) {
      console.log(error);
    } 
};

exports.reorderTask = function(req, res, next) {
    const { id, position} = req.params;
    const urlx = `${domain}/api/v1/Task/${id}?token=${token}`; 
    console.log(urlx);
    
    try {
        let formData = `name=${position}`;

        formData = formData.replace(/\s+/g, '');
        formData = formData.replace(/\_+/g, ' ');
        formData = getUrlVars(formData);


        console.log(id, formData);

        // last - son
        request.post({url:`${domain}/api/v1/Task/${id}?token=${token}`, form: formData}, 
            function(err,httpResponse,body){ 
                if (!err) {
                    res.json(JSON.parse(body));
                }
            }
        );
    } catch(error) {
      console.log(error);
    }
};

exports.postTask = function(req, res, next) {
    const {category,assignedId,createUserId,deadline,objectId,objectName,name } = req.params;
    const urlx = `${domain}/api/v1/Task?token=${token}`; 
//    console.log(urlx);
    
 try {
        let formData = `name=${name}\
                     &assigned[id]=${assignedId}&assigned[objectName]=SevUser\
                     &category[id]=${category}&category[objectName]=Category\
                     & createUser[id]=${createUserId}&createUser[objectName]=SevUser\
                     &noticeCreator=0&deadline=${deadline}&status=100\
                     &object[id]=${objectId}&object[objectName]=${objectName}`;

        formData = formData.replace(/\s+/g, '');
        formData = formData.replace(/\_+/g, ' ');
        formData = getUrlVars(formData);

        console.log(formData);

        // last - son
        request.post({url:`${domain}/api/v1/Task?token=${token}`, form: formData}, 
            function(err,httpResponse,body){ 
                if (!err) {
                    res.json(JSON.parse(body));
                }
            }
        );

    } catch(error) {
      console.log(error);
    } 
};

// duzenlenecek
