var https = require('https');
const { token, domain } = require('../../constant');

exports.account = function(req, res, next) {

    https.get(`${domain}/api/v1/CheckAccountTransaction?limit=1000&offset=0&token=${token}`, 
        function (response) {
        var buffer = "";
    
        response.on("data", function (chunk) {
            buffer += chunk;
        }); 
    
        response.on("end", function (err) {
            const data = JSON.parse(buffer);
            
            var result = [];

            data['objects'].forEach(element => {
                const { id, status, payeePayerName, paymtPurpose, create, amount, feeAmount } = element;
                const tmp = {
                    id, status, payeePayerName, paymtPurpose, create, amount, feeAmount
                };
                result.push(tmp);
            });
            res.json(result);
        }); 
    });
};
