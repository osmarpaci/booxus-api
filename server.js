var customerController = require('./api/controllers/customerController');
var orderController = require('./api/controllers/orderController');
var invoiceController = require('./api/controllers/invoiceController');
var checkAccountTransactionController = require('./api/controllers/checkAccountTransactionController');
var itemController = require('./api/controllers/itemController');
var userController = require('./api/controllers/userController');
var documentController = require('./api/controllers/documentController');
var tagController = require('./api/controllers/tagController');
var taskController = require('./api/controllers/taskController');
var paymentController = require('./api/controllers/paymentController');
var serviceController = require('./api/controllers/serviceController');
var wooController = require('./api/controllers/wooController');
var downloadController = require('./api/controllers/downloadController');

var express = require('express'),
    app = express(),
    port = process.env.PORT || 8090;

var bodyParser = require('body-parser');
var multer = require('multer');
var upload = multer();    

// for parsing application/json
app.use(bodyParser.json()); 

// for parsing application/xwww-
app.use(bodyParser.urlencoded({ extended: true })); 
//form-urlencoded
// for parsing multipart/form-data
app.use(upload.array()); 

app.get("/url", (req, res, next) => {
    
    res.json(["Tony","Lisa","Michael","Ginger","Food"]);
});

app.get("/test", (req, res, next) => {
    
    res.json("test");
});


// resim gosterme
var path = require('path');
var dir = path.join(__dirname, 'public');
app.use(express.static(dir));

app.get('/user/:email/:password', userController.user);

app.get('/customer/:search?', customerController.customer);
app.get('/customer/getAddresses/:id', customerController.getAddresses);
app.get('/customer/getEmail/:id', customerController.getEmail);
app.get('/customer/getCommunicationWays/:id', customerController.getCommunicationWays);
app.get('/customer/getCustomerCategory/:zip/:categ/:str', customerController.getCustomerCategory);
app.get('/customer/getCustomerLocation/:street/:zip/:country', customerController.getCustomerLocation);
app.get('/customer/putCustomer/:id/:text',customerController.putCustomer);
app.get('/order/getOrderPositions/:id', orderController.getOrderPositions);
app.get('/order/:id/:search?', orderController.order);
app.get('/order/getOrder/:id/:type/:stat/:search', orderController.getOrder);
app.get('/order/getNextOrderNumber/:orderType', orderController.getNextOrderNumber);
app.get('/orderc/postOrder/:orderType/:name/:id', orderController.postOrder);
app.get('/order/postOrderMaster/:orderType/:id/:wooId/:name/:addressStreet/:addressZip/:addressCity', orderController.postOrderMaster);
app.get('/order/putcustomerInternalNote/:demandId/:orderId', orderController.putcustomerInternalNote);
app.get('/order/setOrderPos/:orderid/:quantity/:price/:tax/:name/:partid',orderController.setOrderPos);
app.get('/order/putOrderPos/:id/:orderid/:quantity/:price/:tax/:name/:partid',orderController.putOrderPos);
app.get('/order/putOrderPosConn/:id/:orderid',orderController.putOrderPosConn);
app.get('/orderz/rmOrderPos/:id', orderController.rmOrderPos);
app.get('/orderz/rmOrder/:id', orderController.rmOrder);
app.get('/orderz/copyOrder/:id', orderController.copyOrder);
app.get('/orderz/duplicateOrder/:id', orderController.duplicateOrder);

app.get('/order/sendViaEmail/:id/:toEmail/:subject/:text', orderController.sendViaEmail);

app.get('/item/:search?', itemController.item);
app.get('/barkod/:barkod', itemController.barkod);
app.get('/shortCode/:shortCode', itemController.shortCode);
app.get('/barCode/:barCode', itemController.barCode);
app.get('/barCodeFill/:id/:barCode/:priceGross', itemController.barCodeFill);
app.get('/itemCode/:itemId', itemController.itemCode);
app.get('/getStock/:itemId/:maxDate', itemController.getStock);
app.get('/itemCategory/:searchCategory', itemController.XitemCategory);
app.get('/itemInvoicePositions/:search/:type', itemController.itemInvoicePositions);
app.get('/itemOrderPositions/:search/:type/:orderType/:date', itemController.itemOrderPositions);
app.get('/item/putPart/:id/:partNumber/:name/:text/:internalComment',itemController.putPart);
app.get('/item/partBook/:id/:name/:amount/:date/:supplierId/:text',itemController.partBook);
app.get('/item/postPart/:partNumber/:name/:stock/:taxRate/:priceGross/:categoryId', itemController.postPart);

app.get('/invoice/:id/:search?', invoiceController.invoice);
app.get('/invoice/items/getPositions/:id?', invoiceController.getPositions);
app.get('/invoice/items/getPositionsExtended/:id?', invoiceController.getPositionsExtended);
app.get('/invoice/getNextInvoiceNumber/:invoiceType', invoiceController.getNextInvoiceNumber);
app.get('/invoicec/postInvoice/:invoiceType/:id', invoiceController.postInvoice);
app.get('/invoice/postInvoiceMaster/:invoiceType/:id/:name/:addressStreet/:addressZip/:addressCity', invoiceController.postInvoiceMaster);
app.get('/invoice/putPositionText/:id/:partId', invoiceController.putPositionText);

app.get('/document/getOrderPdf/:id', documentController.getOrderPdf);

app.get('/invoice/sendInvoiceEmail/:email/:id', invoiceController.sendInvoiceEmail);
app.get('/invoice/setInvoicePos/:invoiceid/:quantity/:price/:tax/:name',invoiceController.setInvoicePos);
app.get('/invoice/putzInvoicePos/:id/:invoiceid/:quantity/:price/:tax/:name',invoiceController.putInvoicePos);
app.get('/invoicez/rmInvoicePos/:id', invoiceController.rmInvoicePos);
app.get('/invoicez/rmInvoice/:id', invoiceController.rmInvoice);

app.get('/checkAccountTransaction', checkAccountTransactionController.account);

app.get('/payment/getPayments/:name', paymentController.getPayments);
app.get('/payment/getPaymentList/:name/:account', paymentController.getPaymentList);
app.get('/payment/invoice/getinvoiceCheckAccountTransactions/:invoice_id', paymentController.getinvoiceCheckAccountTransactions);

app.get('/tag/tagList/:userTag/:tagType', tagController.getTagList);
app.get('/tag/tagCariList/:userTag/:search', tagController.getTagCariList);
app.get('/tag/postTagRelation/:userTagId/:tagObjectName/:objectId/:objectName', tagController.postTagRelation);
app.get('/tag/tagDetailList/:userTag/:tagType', tagController.getTagDetailList);

app.get('/task/taskItem/:taskId', taskController.getTaskItem);
app.get('/task/taskItemObject/:taskId/:objectId', taskController.getTaskItemObject);
app.get('/task/taskList/:userTask/:taskType', taskController.getTaskList);
app.get('/task/postTask/:category/:assignedId/:createUserId/:deadline/:objectId/:objectName/:name', taskController.postTask);
app.get('/taskz/doneTask/:id', taskController.doneTask);
app.get('/task/reorderTask/:id/:position', taskController.reorderTask);

app.get('/category/:search?', serviceController.getCategory);
app.get('/route/getOptimizedRoute/:origin/:waypoints/:destination', serviceController.getOptimizedRoute);
app.get('/route/getPoi/:search/:lat/:lon/:radius', serviceController.getPoi);
app.get('/xbase64/:search?', serviceController.xbase64);

app.get('/getWoo/:search?', wooController.getWoo);
app.get('/postWoo/:search/:data64', wooController.postWoo);
app.get('/putWoo/:search/:item/:data64', wooController.putWoo);
app.get('/delWoo/:search/:item', wooController.delWoo);

app.post('/download', downloadController.index);

app.listen(port);
console.log('Rest Server Started on: ' + port);