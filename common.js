const request = require('request');
const { token, domain } = require('./constant');

module.exports.makeRequest = function (url) {
//    console.log(url);
    return new Promise((resolve, reject) => {
      request(url, (error, response, body) => {
        if (error) {
          reject(error)
        }
        resolve(JSON.parse(body));
      });
    });
}

module.exports.getDate = function() {
  const today = new Date();
  const dd = String(today.getDate()).padStart(2, '0');
  const mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  const yyyy = today.getFullYear();

  return dd + '.' + mm + '.' + yyyy;
}

module.exports.getUrlVars = function(url) {
  var hash;
  var myJson = {};
  var hashes = url.slice(url.indexOf('?') + 1).split('&');
  for (var i = 0; i < hashes.length; i++) {
      hash = hashes[i].split('=');
      myJson[hash[0]] = hash[1];
  }
  return myJson;
}

module.exports.dynamicSort = function(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        /* next line works with strings and numbers, 
         * and you may want to customize it to your needs
         */
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

module.exports.getDetails = function(url) {
 return new Promise((resolve, reject) => {
      request(url, (error, response, body) => {
        if (error) {
          reject(error)
        }
        resolve(JSON.parse(body));
      });
    });
}

module.exports.Sleep =function (milliseconds) {
   return new Promise(resolve => setTimeout(resolve, milliseconds));
}

